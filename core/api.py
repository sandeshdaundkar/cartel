from django.conf import settings
from django.http.response import HttpResponse
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from common.utils import ForcedResponse, encode_jwt_token
from .exceptions import USER_NOT_FOUND, INVALID_USER_CREDENTIAILS
from .serializers import UserSerializer
from .models import User
from .utils import verify_narcos


class CreateUserView(CreateAPIView):
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects


class VerifyUser(APIView):
    def get(self, request, *args, **kwargs):
        if verify_narcos(kwargs['id']):
            return HttpResponse("<h1> Successfully verified </h1>")
        return HttpResponse("<h1> Failed to verify user </h1>")

class LoginView(RetrieveAPIView):
    serializer_class = UserSerializer

    def get_object(self):
        try:
            return User.objects.get(email=self.request.data.get('email'))
        except User.DoesNotExist:
            raise ForcedResponse(USER_NOT_FOUND)


    def post(self, request, *args, **kwargs ):
        user = self.get_object()
        if not user.validate_password(request.data.get('password')):
            raise ForcedResponse(INVALID_USER_CREDENTIAILS)
        data = self.serializer_class(user).data
        if user.is_verified:
            data['token'] = encode_jwt_token(user, 'narcos')
        return Response(data)
