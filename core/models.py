import datetime
from mongoengine import document
from mongoengine import fields
from hashlib import sha256


# Create your models here.
class User(document.Document):
    first_name = fields.StringField(max_length=30, required=True)
    last_name = fields.StringField(max_length=30, required=True)
    company_name = fields.StringField(max_length=30, required=True, null=True, blank=True)
    email = fields.EmailField(required=True, unique=True)
    is_verified = fields.BooleanField(default=False, required=False)
    password = fields.StringField(required=True)
    mobile_number = fields.StringField(required=False)
    date_joined = fields.DateTimeField(default=datetime.datetime.now)
    read_books = fields.ListField(required=False)

    meta = {'db_alias': 'cartel',
            'collection': 'cartel_members'}

    def __str__(self):
        return "{}".format(self.email)

    def validate_password(self, password):
        if self.password == sha256(password.encode('utf-8')).hexdigest():
            return True
        return False
