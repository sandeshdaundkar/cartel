from django.conf.urls import url
from .api import CreateUserView, LoginView, VerifyUser


urlpatterns = [
    url(r'^register/$', CreateUserView.as_view(), name='create_user'),
    url(r'^verify/(?P<id>[-\w]+)/$', VerifyUser.as_view(), name='verify_narcos'),
    url(r'^login/$', LoginView.as_view(), name='login'),

]