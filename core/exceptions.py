USER_NOT_FOUND = {"message": "User does not exists", "code": 1000011,
                  "code_str": "USER_NOT_FOUND"}

INVALID_USER_CREDENTIAILS = {"message": "User credentials invalid.", "code": 1000012,
                  "code_str": "INVALID_USER_CREDENTIAILS"}

NOT_AUTHORIZED = {"message": "User not authorized.", "code": 1000013,
                  "code_str": "NOT_AUTHORIZED"}

MAX_VERIFY_COUNT_EXCEED = {"message": "Max verification count exceeded.", "code": 1000014,
                  "code_str": "MAX_VERIFY_COUNT_EXCEED"}
