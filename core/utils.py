import json
import uuid
from django.conf import settings
from common.mail import send_verification_mail
from common.utils import setup_cache, close_cache, get_base_url, ForcedResponse
from .constants import NARCOS_EMAIL_VERIFICATION_KEY, VERIFICATION_MAIL_COUNT_KEY
from .exceptions import MAX_VERIFY_COUNT_EXCEED
from .models import User


def send_verification_code(request, user):
    unique_id = str(uuid.uuid4())
    url = get_base_url(request) + "/cartel-admins/verify/" + unique_id
    time = settings.VERIFICATION_EMAIL_EXPIRY * 60 * 60
    key = NARCOS_EMAIL_VERIFICATION_KEY.format(id=unique_id)
    count_key = VERIFICATION_MAIL_COUNT_KEY.format(user_id=user.id.__str__())
    count = 0
    cache = setup_cache()
    d = cache.get(count_key)
    if d:
        count = int(d)
    print("COUNT", d, count)
    if count >= settings.VERIFICATION_EMAIL_COUNT:
        ttl = cache.ttl(count_key)
        raise ForcedResponse(MAX_VERIFY_COUNT_EXCEED,
                             extra_data="Wait {0} minutes {1} seconds before retry.".format(int(ttl/60), int(ttl % 60)))
    data = json.dumps({
        "user_id": user.id.__str__(),
         "type": "narcos",
         "mail_type": "verification"
         })
    cache.setex(key, time, data)
    cache.setex(count_key, count + 1, time)
    close_cache(cache)
    send_verification_mail(user.first_name, user.email, url)


def verify_narcos(unique_id):
    cache = setup_cache()
    key = NARCOS_EMAIL_VERIFICATION_KEY.format(id=unique_id)
    data = cache.get(key)
    resp = False
    if data:
        data = json.loads(data)
        if data.get('type', None) == "narcos" and data.get("mail_type", None) == 'verification':
            try:
                user = User.objects.get(id=data['user_id'])
                user.is_verified = True
                user.save()
                resp = True
                cache.delete(key)
            except User.DoesNotExist:
                pass
    close_cache(cache)
    return resp