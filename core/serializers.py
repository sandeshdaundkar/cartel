from hashlib import sha256
from rest_framework_mongoengine.serializers import DocumentSerializer
from .models import User
from .utils import send_verification_code


class UserSerializer(DocumentSerializer):

    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = (
            'is_verified',
            'date_joined'
        )
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password')
        password = sha256(password.encode('utf-8')).hexdigest()
        validated_data['is_verified'] = False
        user = User.objects.create(password=password, **validated_data)
        send_verification_code(self.context['request'], user)
        return user
