CACHE = {
    "default": {
        "servers": [
            '127.0.0.1:6379',
        ],
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
