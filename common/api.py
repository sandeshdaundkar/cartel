from rest_framework import generics
from .utils import ForcedResponse
from .serializers import BookSerializer, BookRetrieveUpdateDeleteSerializer
from .models import Book
from .cartelExceptions import BOOK_NOT_FOUND
from .permissions import IsNarcos



class ListCreateBookView(generics.ListCreateAPIView):
    serializer_class = BookSerializer
    permission_classes = (IsNarcos, )

    def get_queryset(self):
        return Book.objects


class RetrieveUpdateDestroyBookView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = BookRetrieveUpdateDeleteSerializer

    def get_object(self):
        try:
            return Book.objects.get(id=self.kwargs.get('book_id'))
        except Book.DoesNotExist:
            raise ForcedResponse(BOOK_NOT_FOUND)
