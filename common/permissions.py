from rest_framework.permissions import BasePermission
from common.utils import ForcedResponse
from core.exceptions import NOT_AUTHORIZED


class IsNarcos(BasePermission):

    def has_permission(self, request, view):
        if hasattr(request, 'narcos'):
            return True
        raise ForcedResponse(NOT_AUTHORIZED)

