from rest_framework_mongoengine import serializers
from .models import Book


class BookSerializer(serializers.DocumentSerializer):
    class Meta:
        model = Book
        fields = '__all__'


class BookRetrieveUpdateDeleteSerializer(serializers.DocumentSerializer):
    class Meta:
        model = Book
        fields = '__all__'
        read_only_fields = ('id', 'name', 'email', 'author', 'isbn')


    def update(self, instance, validated_data):
        instance.price = self.validated_data.get('price')
        return instance

