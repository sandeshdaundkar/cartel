from django.db import models
from mongoengine.document import Document
from mongoengine import fields, DoesNotExist

# Create your models here.
class Book(Document):
    name = fields.StringField(max_length=255, unique=True)
    email = fields.EmailField(unique=True)
    price = fields.IntField()
    author = fields.StringField(max_length=255)
    isbn = fields.StringField()

    meta = {'db_alias': 'cartel', 'collection': 'cartel_books'}

    def __str__(self):
        return "{}".format(self.email)

