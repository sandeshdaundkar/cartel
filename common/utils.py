import os
import sys
import jwt
import datetime
from django.conf import settings
from django.http.response import JsonResponse
from rest_framework import status
from .cartelExceptions import JWT_TOKEN_EXPIRED, INVALID_JWT_TOKEN, REDIS_CONFIG_ERROR


class ForcedResponse(Exception):
    def __init__(self, response, extra_data="", status_code=status.HTTP_200_OK):
        response = response.copy()
        if extra_data or not response.get("extra_data", None):
            response["extra_data "] = extra_data
        self.response = JsonResponse({"status": 'error', "data": response})
        self.response.error = True
        self.response.status_code = status_code


def encode_jwt_token(user, role='consumer'):
    expiration_delay = 120 if role=='consumer' else settings.JWT_TOKEN_EXPIRATION[role] # TODO: change from settings
    expiration = datetime.datetime.now() + datetime.timedelta(seconds=expiration_delay)
    payload = {
        'user': user.id.__str__(),
        'expiration': expiration.timestamp(),
        'role': role
    }
    return jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256').decode('utf-8')


def decode_jwt_token(token):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
        if datetime.datetime.utcfromtimestamp(payload['expiration']) < datetime.datetime.now():
            raise ForcedResponse(JWT_TOKEN_EXPIRED)
        return payload['user'], payload['role']
    except Exception as e:
        if isinstance(e, ForcedResponse):
            raise e
        raise ForcedResponse(INVALID_JWT_TOKEN)


def setup_cache(cache_id="default"):
    cache_settings = settings.CACHE.get(cache_id, None)

    if cache_settings is None:
        raise ForcedResponse(REDIS_CONFIG_ERROR, extra_data="Cache configuration incomplete")
    servers = cache_settings.get('servers', None)

    if servers is None:
        raise ForcedResponse(REDIS_CONFIG_ERROR, extra_data="Cache configuration incomplete")

    if len(servers)  > 1:
        raise ForcedResponse(REDIS_CONFIG_ERROR, extra_data="Redis servers > 1 not supported")

    s = servers[0]
    host, port = s.split(':')
    import redis
    if hasattr(redis, "client"):
        cache = redis.client.Redis(host, int(port))
    else:
        cache = redis.Redis(host, int(port))

    return cache


def close_cache(cache):
    if cache:
        if hasattr(cache, "disconnect"):
            cache.disconnect()
        elif hasattr(cache, "disconnect_all"):
            cache.disconnect_all()
        elif hasattr(cache, "close"):
            cache.close()
        del cache


def get_base_url(request):
    scheme = "https://" if request.is_secure() else "http://"
    url = scheme + request.META["HTTP_HOST"]
    return url


# def get_env(key, default=None):
#     if default is None:
#         try:
#             return os.environ[key]
#         except KeyError:
#             print("ERROR: Missing Key {}".format(key))
#             sys.exit(1)
#     return os.environ.get(key, default)
