from django.conf.urls import url
from .api import ListCreateBookView, RetrieveUpdateDestroyBookView

urlpatterns = [
    url(r'^book/create/', ListCreateBookView.as_view(), name='create_book'),
    url(r'^book/list/', ListCreateBookView.as_view(), name='list_books'),
    url(r'^book/retrieve/(?P<book_id>\w+)/', RetrieveUpdateDestroyBookView.as_view(), name='retrieve_book'),
    url(r'^book/update/(?P<book_id>\w+)/', RetrieveUpdateDestroyBookView.as_view(), name='update_book'),
    url(r'^book/delete/(?P<book_id>\w+)/', RetrieveUpdateDestroyBookView.as_view(), name='delete_book'),

]