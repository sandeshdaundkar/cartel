import logging
import traceback
from django.utils.deprecation import MiddlewareMixin
from mongoengine.errors import ValidationError
from core.exceptions import USER_NOT_FOUND
from .cartelExceptions import UNKNOWN_ERROR, SERIALIZATION_ERROR, JWT_FORMAT_ERROR
from .utils import ForcedResponse, decode_jwt_token


class CustomMiddleware(MiddlewareMixin):

    def process_request(self, request):
        print("First middleware executed while processing request. <{}>".format(self.__class__.__name__))
        # return HttpResponse("some response")

    def process_response(self, request, response):
        print("Second middleware executed while processing response. <{}>".format(self.__class__.__name__))
        return response


class AnotherMiddleware(MiddlewareMixin):

    def process_request(self, request):
        print("Second middleware executed while processing request. <{}>".format(self.__class__.__name__))

    def process_response(self, request, response):
        print("First middleware executed while processing response. <{}>".format(self.__class__.__name__))
        return response


class ForcedResponseMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    @staticmethod
    def process_exception(request, response):
        logger = logging.getLogger('django')

        if isinstance(response, ForcedResponse):
            log_message = "\"{0} {1}\" {2}\n{3}".format(request.method, request.get_full_path(), 200,
                                                        traceback.format_exc())
            print(log_message)
            return response.response


class ResponseFormatMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if response.status_code == 500:
            return ForcedResponse(UNKNOWN_ERROR).response
        if response.status_code == 400:
            try:
                key = list(response.data.keys())[0]
                msg = "Error for field '{}'".format(key)
                data = response.data[key][0]
            except (KeyError, AttributeError):
                msg = 'Data Format Error'
                data = response.data
            return ForcedResponse(SERIALIZATION_ERROR, extra_data="{}. - {}".format(msg, data)).response

        try:
            if (not getattr(response, 'error', False)) and (isinstance(response.data, dict) or
                isinstance(response.data, list)):

                content = '{"status": "success", "data": ' + response.content.decode('utf-8') + '}'
                response.content = content.encode('utf-8')
                response.data = {'status': 'success', 'data': response.data}
        except AttributeError:
            pass
        return response


class UserAuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response


    def __call__(self, request):
        from core.models import User
        jwt_token = request.META.get('HTTP_AUTHORIZATION', None)
        if jwt_token:
            try:
                user, role = decode_jwt_token(jwt_token)
                if role == 'narcos':
                    request.narcos = User.objects.get(id=user)
                else:
                    return ForcedResponse(JWT_FORMAT_ERROR).response
            except (User.DoesNotExist, ValidationError):
                return ForcedResponse(USER_NOT_FOUND).response
            except ValueError:
                return ForcedResponse(JWT_FORMAT_ERROR).response

        response = self.get_response(request)
        return response
