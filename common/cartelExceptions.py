SERIALIZATION_ERROR = {"message": "Bad Data", "code": 1000098,
                       "code_str": "ERR_SERIALIZATION_ERROR"}

UNKNOWN_ERROR = {"message": "Unknown Error", "code": 1000099,
                 "code_str": "ERR_UNKNOWN"}



BOOK_NOT_FOUND = {"message": "Book does not exists", "code": 1000001,
                  "code_str": "BOOK_NOT_FOUND"}

JWT_TOKEN_EXPIRED = {"message": "Token is expired. Login Again.", "code": 1000002,
                  "code_str": "JWT_TOKEN_EXPIRED"}

INVALID_JWT_TOKEN = {"message": "Token is invalid.", "code": 1000003,
                  "code_str": "INVALID_JWT_TOKEN"}

JWT_FORMAT_ERROR = {"message": "Token format is invalid.", "code": 1000004,
                  "code_str": "JWT_FORMAT_ERROR"}

REDIS_CONFIG_ERROR = {"message": "Redis is not properly configured for caching.", "code": 1000004,
                  "code_str": "REDIS_CONFIG_ERROR"}

