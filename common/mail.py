import json
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import get_template
from django.utils.html import strip_tags


def send_verification_mail(name, email, url, retry=5):
    context = {
        "name": name,
        "url": url,
        "platform_name": settings.NAME,
        "expiry": settings.VERIFICATION_EMAIL_EXPIRY
    }
    from_email = "{0} <{1}>".format(settings.NAME, settings.EMAIL_HOST_USER)
    subject = '{0} email verification'.format(settings.NAME)
    html = get_template('email_verification.html').render(context)
    message = strip_tags(html)
    try:
        send_mail(subject, message, from_email, [email], html_message=html, fail_silently=False)
    except Exception:
        if retry > 0:
            send_verification_mail(name, email, url, retry - 1)
